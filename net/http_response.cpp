#include "http_response.h"
#include "logging/logger.h"
#include "pub/stringex.h"
#include "pub/memex.h"
#include "http_handle.h"
#include <sstream>
#include <stdio.h>

namespace sw_server {

http_response::http_response(std::string remote_addr, boost::asio::ip::tcp::socket& socket, http_handle& handle_obj):_socket(socket)
    , _handle_obj(handle_obj)
    , _status("200 OK")
    , _status_code(HTTP_OK_STATUS)
    , _proto("HTTP/1.0")
    , _proto_major(1)
    , _proto_minor(0)
    , _content_length(0)
    , _remote_addr(remote_addr){

}

http_response::~http_response() {

}

std::unordered_map<std::string, std::vector<std::string>>& http_response::headers() {
    return _headers;
}

void http_response::writestring(const std::string outputInfo) {
    auto ret_ptr = MAKE_SHARED_ARRAY((std::size_t)outputInfo.size());

    memcpy((char*)ret_ptr->_data_p, outputInfo.c_str(), outputInfo.size());
    write(ret_ptr);
    //InfoLogf("http_response writestring:%s", outputInfo.c_str());
}

void http_response::write(std::shared_ptr<SHARE_ARRAY> data_ptr) {
	std::string header_string;
    std::stringstream header_stream;

    _status = get_statuscode_string();

    header_stream << "HTTP/" << _proto_major << "." << _proto_minor << " " << _status_code << " " << _status << "\r\n";

    bool is_content_len = false;
	for (auto item : _headers) {
        std::string key = item.first;
        std::vector<std::string> info_vec = item.second;

        std::string key_lower = string_lower(key);

        if (key_lower.compare("content-length") == 0) {
            is_content_len = true;
        }
        header_stream << key << ":";
        for (int index = 0; index < info_vec.size(); index++) {
			if (index == 0) {
				header_stream << " " << info_vec[index];
			} else {
				header_stream << "," << info_vec[index];
			}
		}
		header_stream << "\r\n";
	}

    if (!is_content_len) {
        header_stream << "Content-Length: " << data_ptr->_data_size << "\r\n";
    }
	header_stream << "\r\n";
    header_string = header_stream.str();
	//InfoLogf("send httpheader:\r\n%s", header_string.c_str());
    
    int total = header_string.size() + data_ptr->_data_size;
    auto write_data_ptr = MAKE_SHARED_ARRAY(total);
	unsigned char* data_p = write_data_ptr->_data_p;
    
    int offset = sprintf((char*)data_p, "%s", header_string.c_str());

    memcpy(data_p + offset, data_ptr->_data_p, data_ptr->_data_size);

    _handle_obj.async_send(write_data_ptr);
    return;
}

std::string http_response::get_statuscode_string() {
    std::string ret_string("inter error");

	if (_status_code == HTTP_OK_STATUS) {
		ret_string = "OK";
	} else if (_status_code == HTTP_NOT_EXIST_STATUS) {
		ret_string = "Not Found";
	}
	return ret_string;
}

void http_response::set_statuscode(int status_code) {
    _status_code = status_code;
}

}