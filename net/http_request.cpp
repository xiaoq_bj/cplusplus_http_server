#include "http_request.h"
#include "pub/stringex.h"
#include "logging/logger.h"
#include "pub/memex.h"
#include <vector>

namespace sw_server {
http_request::http_request():_proto_major(1)
    , _proto_minor(0)
    , _content_length(0) {

}

http_request::~http_request() {

}

int http_request::parse(std::string& header_str, std::string remote_addr) {
    std::vector<std::string> line_vec;
    std::string header_info;
    const std::string CONTENT_LENGTH("content-length");
    const std::string HOST("host");

    int end_pos = header_str.find("\r\n\r\n");
    if (end_pos != header_str.npos) {
        header_info = header_str.substr(0, end_pos);
    } else {
        header_info = header_str;
    }
    string_split(header_info, "\r\n", line_vec);

    int index = 0;
    for (auto line_str:line_vec) {
        if (index == 0) {
            std::vector<std::string> info_vec;
            string_split(line_str, " ", info_vec);
            if (info_vec.size() != 3) {
                ErrorLogf("http header error:%s", line_str.c_str());
                return -1;
            }
            _method = info_vec[0];
            std::string subpath = info_vec[1];
            _proto = info_vec[2];
            
            int pos = subpath.find("?");
            if (pos != subpath.npos) {
                _url = subpath.substr(0, pos);
                std::string param_str = subpath.substr(pos+1);
                parse_param(param_str);
            } else {
                _url = subpath;
            }
            //InfoLogf("method=%s, subpath=%s, proto=%s", _method.c_str(), _url.c_str(), _proto.c_str());
        } else {
            std::string key_str;
            std::vector<std::string> content_vec;
            int info_count = get_header_info(line_str, key_str, content_vec);
            if (info_count <= 0) {
                ErrorLogf("http header error:%s", line_str.c_str());
                return -1;
            }
            _headers.insert(std::make_pair(key_str, content_vec));
            std::string key_lower = string_lower(key_str);
            if (key_lower.compare(CONTENT_LENGTH) == 0) {
                std::string content_info = content_vec[0];
                _content_length = std::atoi(content_info.c_str());
                _body_ptr = MAKE_SHARED_ARRAY(_content_length+1);
                //DebugLogf("content_length_str:%s, int:%d", content_info.c_str(), _content_length);
            } else if (key_lower.compare(HOST) == 0) {
                _host = content_vec[0];
                //DebugLogf("host:%s", _host.c_str());
            }
        }
        index++;
    }

    _remoteaddr = remote_addr;
    return _headers.size();
}

int http_request::parse_param(std::string& param_str) {
    std::vector<std::string> param_vec;
    int param_count = string_split(param_str, "&", param_vec);

    if (param_count <= 0) {
        ErrorLogf("param count=%d, param=%s", param_count, param_str.c_str());
        return param_count;
    }

    for (auto param_item : param_vec) {
        int pos = param_item.find("=");
        if (pos == param_item.npos) {
            continue;
        }
        std::string key = param_item.substr(0, pos);
        std::string value = param_item.substr(pos+1);
        std::vector<std::string> item_vec;

        string_split(value, ",", item_vec);
        _param_map.insert(std::make_pair(key, item_vec));
    }

    return param_count;
}

int http_request::body_parse(unsigned char* body_p) {
    return 0;
}

int http_request::get_header_info(const std::string& input_str, std::string& key_str, std::vector<std::string>& content_vec) {
    std::string line_info(input_str);

    int pos = line_info.find(":");
    if (pos == line_info.npos) {
        return 0;
    }
    key_str = line_info.substr(0, pos);
    std::string content_str = line_info.substr(pos+1);
    
    pos = content_str.find(" ");
    if (pos == 0) {
        content_str = content_str.substr(1);
    }

    string_split(content_str, ",", content_vec);

    return content_vec.size();
}

}