#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H
#include "pub/memex.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <memory>

namespace sw_server {
class http_request {
public:
    http_request();
    ~http_request();
    
    int parse(std::string& header_str, std::string remote_addr);
    int body_parse(unsigned char* body_p);

private:
    int get_header_info(const std::string& input_str, std::string& key_str, std::vector<std::string>& content_list);
    int parse_param(std::string& param_str);

public:
    std::string _method;
    std::string _url;
    std::string _proto;
    int _proto_major;
    int _proto_minor;
    std::unordered_map<std::string, std::vector<std::string>> _headers;
    std::shared_ptr<SHARE_ARRAY> _body_ptr;
    int _content_length;
    std::string _host;
    std::unordered_map<std::string, std::vector<std::string>> _param_map;
    std::string _remoteaddr;
};
}
#endif//HTTP_REQUEST_H