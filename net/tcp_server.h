#ifndef TCP_SERVER_H
#define TCP_SERVER_H
#include "sys/thread_pool.h"
#include "http_handle.h"
#include "pub/mathex.h"
#include <boost/asio.hpp>
#include <list>
#include <unordered_map>
#include <mutex>

namespace sw_server {
class tcp_server {
public:
    tcp_server(int port, int proc_max=std::thread::hardware_concurrency());
    ~tcp_server();

    void run();
private:
    std::shared_ptr<http_handle> create_http_handle();
    void accept();
    void recycle_connid(int connid);
    void recycle_all_connid();
    void insert_remove_connid(int connid);
    bool get_remove_connid(int& connid);

    #define GET_IOS_PTR _ios_vec[GET_RANDOM(_ios_vec.size())]

private:
    int _port;
    int _proc_max;
    std::vector<std::shared_ptr<boost::asio::io_service>> _ios_vec;
    std::vector<std::shared_ptr<boost::asio::io_service::work>> _work_vec;
    std::shared_ptr<boost::asio::ip::tcp::acceptor> _acceptor_ptr;
    Thread_Pool _thread_pool;
    std::list<int> _connid_list;
    std::list<int> _remove_connid_list;
    std::unordered_map<int, std::shared_ptr<http_handle>> _handlers_map;
    std::mutex _mutex;
    std::mutex _remove_mutex;
};
}
#endif//TCP_SERVER_H

