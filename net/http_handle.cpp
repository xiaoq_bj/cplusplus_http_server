#include "http_handle.h"
#include "logging/logger.h"
#include "tcp_server.h"

namespace sw_server {
const int RCV_LEN = 1;

std::map<std::string, std::function<void(http_request&, std::shared_ptr<http_response>)>> http_handle::_handle_func_map;

http_handle::http_handle(boost::asio::io_service& ios, int connid, tcp_server& server_obj):_server_obj(server_obj),
    _socket(ios),
    _offset(0),
    _connid(connid) {

}

http_handle::~http_handle() {
    //InfoLogf("http_handle destruct: connid=%d", _connid);
}

void http_handle::rcv_http_header() {
    _offset = 0;
    boost::asio::async_read_until(_socket, _stream_buf,
        "\r\n\r\n", [this](const boost::system::error_code& ec, size_t size) {
            if (ec) {
                ErrorLogf("rcv error(%d:%s), size=%d.", ec.value(), ec.message().c_str(), size);
                handle_error(ec);
                return;
            }
            if (ec == boost::asio::error::operation_aborted) {
                ErrorLogf("rcv_http_header async_read error:%d, %s", ec.value(), ec.message().c_str());
                handle_error(ec);
                return;
            }
            _offset += size;
            if (_offset >= HEADER_BUFFER_SIZE) {
                ErrorLogf("rcv_http_header size=%d is too large", _offset);
                handle_error(ec);
                return;
            }
    
            auto cbt = _stream_buf.data();
            std::string http_info_str(boost::asio::buffers_begin(cbt), boost::asio::buffers_end(cbt));
            //InfoLogf("get http all data:\r\n%s", http_info_str.c_str());
            int pos = http_info_str.find("\r\n\r\n");
            std::string http_header_str;
            if (pos != http_info_str.npos) {
                http_header_str = http_info_str.substr(0, pos);
            } else {
                ErrorLogf("rcv error(%d:%s), size=%d, data=%s", ec.value(), ec.message().c_str(), size, http_info_str.c_str());
                handle_error(ec);
                return;
            }
            //InfoLogf("get http header:\r\n%s", http_header_str.c_str());
            int ret = set_http_header(http_header_str);
            if (ret <= 0) {
                handle_error(ec);
                return;
            }

            if (_request._content_length > 0) {
                std::string http_body_str = http_info_str.substr(pos+strlen("\r\n\r\n"));
                //InfoLogf("http body string:%s, size=%d", http_body_str.c_str(), http_body_str.size());
                memcpy(_request._body_ptr->_data_p, http_body_str.c_str(), http_body_str.size());
            }

            exec_http_handle();
        });
}

int http_handle::set_http_header(std::string& header_str) {
    std::string remote_ip = _socket.remote_endpoint().address().to_string();
    int remote_port = _socket.remote_endpoint().port();

    std::string remote_addr = remote_ip + ":";
    remote_addr += std::to_string(remote_port);

    int ret = _request.parse(header_str, remote_addr);
    if (ret > 0) {
        //DebugLogf("http header:\r\nMethod=%s, url=%s, proto=%s", 
        //          _request._method.c_str(), _request._url.c_str(), _request._proto.c_str());
        for (auto it: _request._headers) {
            std::string line_str = it.first;
            std::vector<std::string> content_vec = (std::vector<std::string>)it.second;
            for (auto content: content_vec) {
                line_str = line_str + " ";
                line_str = line_str + content;
            }
            //DebugLogf("%s, vec_size=%d, %s", line_str.c_str(), content_vec.size(), content_vec[0].c_str());
        }
        //DebugLogf("content_length=%d, host=%s, remoteaddr=%s", _request._content_length, _request._host.c_str(), _request._remoteaddr.c_str());
    }

    return ret;
}

void http_handle::rcv_http_body() {
    char* buffer_p = (char*)_request._body_ptr->_data_p;

    //InfoLogf("rcv_http_body content_length=%d", _request._content_length);
    boost::asio::async_read(_socket, boost::asio::buffer(buffer_p, _request._content_length), 
                            boost::asio::transfer_exactly(_request._content_length), 
                            [this](const boost::system::error_code& ec, size_t size){
        if (ec == boost::asio::error::operation_aborted) {
            ErrorLogf("rcv_http_body async_read operation_aborted error:%d, %s", ec.value(), ec.message().c_str());
            handle_error(ec);
            return;
        }
        char* buffer_p = (char*)_request._body_ptr->_data_p;

        buffer_p[_request._content_length] = 0;
        //InfoLogf("http body data:%s", buffer_p);

        exec_http_handle();
    });
}

int http_handle::exec_http_handle() {
    boost::system::error_code ec;

    //InfoLogf("exec_http_handle subpath:%s", _request._url.c_str());
    std::function<void(http_request&, std::shared_ptr<http_response>)> handle_func;
    bool is_exist = get_handle_func(_request._url, handle_func);
    if (!is_exist) {
        ErrorLogf("rcv_http_body handle function not exist:%d, %s", ec.value(), ec.message().c_str());
        handle_error(ec);
        return -1;
    }
    std::string remote_ip = _socket.remote_endpoint().address().to_string();
    int remote_port = _socket.remote_endpoint().port();

    std::string remote_addr = remote_ip + ":";
    remote_addr += std::to_string(remote_port);

    _response_ptr = std::make_shared<http_response>(remote_addr, _socket, *this);
    handle_func(_request, _response_ptr);
    return 0;
}

void http_handle::handle_read() {
    rcv_http_header();
}

void http_handle::handle_error(const boost::system::error_code& ec) {
    //InfoLogf("close_socket...connid=%d", _connid);
    close_socket();
    _error_call_back(_connid);
}

void http_handle::close_socket() {
    boost::system::error_code ec;
    _socket.shutdown(boost::asio::ip::tcp::socket::shutdown_send, ec);
    _socket.close(ec);
}

void http_handle::async_send(std::shared_ptr<SHARE_ARRAY> array_ptr) {
    bool write_in_progress = !_packet_queue.empty();
    _packet_queue.push(array_ptr);
    if (!write_in_progress) {
        do_async_send();
    }
    return;
    //_server_obj.get_ios().post([array_ptr, this](){
    //    bool write_in_progress = !_packet_queue.empty();
    //    _packet_queue.push(array_ptr);
    //    if (!write_in_progress) {
    //        do_async_send();
    //    }
    //    return;
    //});
}

void http_handle::do_async_send() {
    if (_packet_queue.empty()) {
        return;
    }
    auto item = _packet_queue.front();

    char* data_p = (char*)item->_data_p;
    int send_len = item->_data_size;

    //InfoLogf("do_async_send datalen=%d", send_len);
    _socket.async_send(boost::asio::buffer(data_p, send_len),
        std::bind(&http_handle::handle_send, this, std::placeholders::_1, std::placeholders::_2));

    return;
}

void http_handle::handle_send(const boost::system::error_code& ec, std::size_t sent_size) {
    if (ec == boost::asio::error::operation_aborted) {
        ErrorLogf("handle_send handle_send operation_aborted error:%d, %s", ec.value(), ec.message().c_str());
        handle_error(ec);
        return;
    }
    handle_error(ec);
    //InfoLogf("handle_send return=%d", sent_size);
    /*
    assert(!_packet_queue.empty());
    if (_packet_queue.empty()) {
        ErrorLogf("handle_send Asio error since send queue empty inside handle send. bytes_sent:%d", sent_size);
        return;
    }

    _packet_queue.pop();
    if (!_packet_queue.empty()) {
        do_async_send();
    } else {
        handle_error(ec);
    }
    */
    return;
}
}