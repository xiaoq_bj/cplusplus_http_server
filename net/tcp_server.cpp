#include "tcp_server.h"
#include "logging/logger.h"
#include <numeric> 

namespace sw_server {
const int TCP_CONN_MAX = 65535;

tcp_server::tcp_server(int port, int proc_max):_port(port)
    ,_proc_max(proc_max)
    ,_thread_pool(proc_max) {
    _connid_list.resize(TCP_CONN_MAX);
    std::iota(_connid_list.begin(), _connid_list.end(), 1);

    for (int index = 0; index < proc_max; index++) {
        auto ios_ptr = std::make_shared<boost::asio::io_service>();
        std::shared_ptr<boost::asio::io_service::work>  work_ptr(new boost::asio::io_service::work(*(ios_ptr.get())));
	    _work_vec.push_back(work_ptr);
        _ios_vec.push_back(ios_ptr);
    }

    _acceptor_ptr = std::make_shared<boost::asio::ip::tcp::acceptor>(*(_ios_vec[0].get()), boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port));

}

tcp_server::~tcp_server() {

}

void tcp_server::run() {
    _thread_pool.Start();
    InfoLog("tcp_server is running.");
    _acceptor_ptr->listen(boost::asio::socket_base::max_listen_connections);
    InfoLogf("boost asio acceptor max_listen_connections=%d", boost::asio::socket_base::max_listen_connections);
    accept();

    for (int index = 0; index < _proc_max; index++) {
        _thread_pool.enqueue([this, index]{
            InfoLogf("ioservice index(%d) is running.", index);
            _ios_vec[index]->run();
        });
    }
}

void tcp_server::accept() {
    auto http_handle_ptr = create_http_handle();

    //InfoLogf("tcp_server accept is running, connid=%d", http_handle_ptr->get_connid());
    _acceptor_ptr->async_accept(http_handle_ptr->get_read_socket(), [this, http_handle_ptr](const boost::system::error_code& err) {
        if (err) {
            ErrorLogf("accept error:%d, %s", err.value(), err.message().c_str());
            return;
        }
        if(err != boost::asio::error::operation_aborted) {
            accept();
        }

        _mutex.lock();
        _handlers_map.insert(std::make_pair(http_handle_ptr->get_connid(), http_handle_ptr));
        _mutex.unlock();
        //start read handle
        //InfoLogf("tcp server accept ok, connid=%d", http_handle_ptr->get_connid());
        http_handle_ptr->handle_read();
    });
    recycle_all_connid();
}

std::shared_ptr<http_handle> tcp_server::create_http_handle() {
    _mutex.lock();
    int connid = _connid_list.front();
    _connid_list.pop_front();
    _mutex.unlock();

    auto ios_ptr = GET_IOS_PTR;
    std::shared_ptr<http_handle> http_handle_ptr = std::make_shared<http_handle>(*(ios_ptr.get()), connid, *this);

    //InfoLogf("create_http_handle connid=%d", connid);
    http_handle_ptr->set_callback_error([this](int connid){
        //InfoLogf("tcp read error: connid=%d", connid);
        insert_remove_connid(connid);
    });
    return http_handle_ptr;
}

void tcp_server::insert_remove_connid(int connid) {
    std::lock_guard<std::mutex> guard(_remove_mutex);
    _remove_connid_list.push_back(connid);
}

bool tcp_server::get_remove_connid(int& connid) {
    std::lock_guard<std::mutex> guard(_remove_mutex);
    if (_remove_connid_list.empty()) {
        return false;
    }

    connid = _remove_connid_list.front();
    _remove_connid_list.pop_front();

    return true;
}

void tcp_server::recycle_all_connid() {
    bool is_exist = false;

    do {
        int connid = 0;
        is_exist = get_remove_connid(connid);

        if (is_exist) {
            recycle_connid(connid);
        }
    } while(is_exist);
}

void tcp_server::recycle_connid(int connid) {
    std::lock_guard<std::mutex> guard(_mutex);

    auto it = _handlers_map.find(connid);
    if (it != _handlers_map.end()) {
        _handlers_map.erase(it);
    } else {
        return;
    }
    _connid_list.push_back(connid);
}
}