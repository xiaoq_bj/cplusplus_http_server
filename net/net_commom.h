#ifndef NET_COMMON_H
#define NET_COMMON_H

namespace sw_server {
class net_handle {
public:
    virtual void handle_read() = 0;
};
}
#endif//NET_COMMON_H