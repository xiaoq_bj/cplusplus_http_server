#ifndef HTTP_RESPONSE_H
#define HTTP_RESPONSE_H
#include <boost/asio.hpp>
#include <string>
#include <unordered_map>
#include "pub/memex.h"

namespace sw_server {

static const int HTTP_OK_STATUS = 200;
static const int HTTP_NOT_EXIST_STATUS = 404;

class http_handle;

class http_response {
public:
    http_response(std::string remote_addr, boost::asio::ip::tcp::socket& socket, http_handle& handle_obj);
    ~http_response();

    std::unordered_map<std::string, std::vector<std::string>>& headers();
    void write(std::shared_ptr<SHARE_ARRAY> data_ptr);
    void writestring(const std::string outputInfo);
    void set_statuscode(int status_code);

private:
    std::string get_statuscode_string();

private:
    const boost::asio::ip::tcp::socket& _socket;
    http_handle& _handle_obj;
    std::string _status;
    unsigned int _status_code;
    std::string _proto;
    int _proto_major;
    int _proto_minor;
    std::unordered_map<std::string, std::vector<std::string>> _headers;
    int _content_length;
    std::string _remote_addr;
};

}
#endif//HTTP_RESPONSE_H