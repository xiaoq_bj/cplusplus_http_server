#ifndef HTTP_HANDLE_H
#define HTTP_HANDLE_H
#include "net_commom.h"
#include "http_request.h"
#include "http_response.h"
#include "pub/memex.h"
#include <boost/asio.hpp>
#include <map>
#include <queue>

namespace sw_server {
#define HEADER_BUFFER_SIZE (8*1024)

class SEND_DATA{
public:
    SEND_DATA();
    SEND_DATA(const SEND_DATA& data) {
        _buffer_ptr = data._buffer_ptr;
        _pos = data._pos;
        _len = data._len;
    }
    
    SEND_DATA(std::shared_ptr<char> buffer_ptr, int pos, int len):_buffer_ptr(buffer_ptr)
        ,_pos(pos)
        ,_len(len){

        }
    ~SEND_DATA(){

    }
public:
    std::shared_ptr<char> _buffer_ptr;
    int _pos;
    int _len;
};

class tcp_server;

class http_handle : public net_handle {
public:
    http_handle(boost::asio::io_service& ios, int connid, tcp_server& server_obj);
    ~http_handle();

    void handle_read();

    template<typename F>
    void set_callback_error(F f) {
        _error_call_back = f;
    }

    int get_connid() {
        return _connid;
    }

    boost::asio::ip::tcp::socket& get_read_socket() {
        return _socket;
    }

    void async_send(std::shared_ptr<SHARE_ARRAY> array_ptr);
    static void add_subpath(std::string subpath, std::function<void(http_request&, std::shared_ptr<http_response>)> handle_func) {
        _handle_func_map.insert(std::make_pair(subpath, handle_func));
    }
private:
    void rcv_http_header();
    int set_http_header(std::string& header_str);
    void rcv_http_body();

    void handle_error(const boost::system::error_code& ec);
    void close_socket();
    void do_async_send();
    void handle_send(const boost::system::error_code& ec, std::size_t sent_size);

    static bool get_handle_func(std::string subpath, std::function<void(http_request&, std::shared_ptr<http_response>)>& handle_func) {
        auto iter = _handle_func_map.find(subpath);
        if (iter == _handle_func_map.end()) {
            return false;
        }

        handle_func = (std::function<void(http_request&, std::shared_ptr<http_response>)>)(iter->second);
        return true;
    }
    int exec_http_handle();
private:
    tcp_server& _server_obj;
    boost::asio::ip::tcp::socket _socket;
    boost::asio::streambuf _stream_buf;
    std::size_t _offset;
    std::function<void(int connid)> _error_call_back;
    int _connid;
    http_request _request;
    std::shared_ptr<http_response> _response_ptr;

    std::queue<std::shared_ptr<SHARE_ARRAY>> _packet_queue;
    static std::map<std::string, std::function<void(http_request&, std::shared_ptr<http_response>)>> _handle_func_map;
};

}

#endif//HTTP_HANDLE_H