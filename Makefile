CC = gcc  
C++ = g++  
LINK = g++  

LIBS = -L/usr/local/lib -pthread -Wl,-Bstatic -llog4cplus -lboost_thread -lboost_system -Wl,-Bdynamic

#must add -fPIC option  
CCFLAGS = $(COMPILER_FLAGS) -c -g  -fPIC -Wall
C++FLAGS = $(COMPILER_FLAGS) -c -g -fPIC -Wall -std=c++11
  
TARGET=http_server_demo
  
INCLUDES = -I. -I./sys -I./net -I./logging -I./rapidjson -I./rapidjson/include


C++FILES = ./sys/thread_pool.cpp ./http_server_demo.cpp ./net/http_handle.cpp ./net/tcp_server.cpp ./net/http_request.cpp ./net/http_response.cpp
CFILES = 

OBJFILE = $(CFILES:.c=.o) $(C++FILES:.cpp=.o)  

all:$(TARGET)  
  
$(TARGET): $(OBJFILE)  
	$(LINK) $^ -Wall $(LIBS) -o $@
 
%.o:%.c  
	$(CC) -o $@ $(CCFLAGS) $< $(INCLUDES)  
  
%.o:%.cpp  
	$(C++) -o $@ $(C++FLAGS) $< $(INCLUDES)  
  
  
clean:  
	rm -rf $(TARGET)  
	rm -rf $(OBJFILE)  


