#ifndef MEMEX_H
#define MEMEX_H
#include "logging/logger.h"
#include <memory>
#include <string.h>
#include <assert.h>

namespace sw_server {

typedef struct _SHARE_ARRAY{
    unsigned char* _data_p;
    int _data_size;
    const char* _filename;
    int _fileline;
}SHARE_ARRAY;

#define MAKE_SHARED_ARRAY(size) make_shared_array(size, __FILE__, __LINE__)

inline void delete_array(SHARE_ARRAY* array_p) {
    unsigned char* data_p = array_p->_data_p;
    int size = array_p->_data_size;

    if ((data_p[size+3] != 'd') || (data_p[size+2] != 'c') 
    || (data_p[size+1] != 'b') || (data_p[size] != 'a')) {
        ErrorLogf("delete memory error: size=%d, %s:%d", 
            array_p->_data_size, array_p->_filename, array_p->_fileline);
        assert(0);
    }

    delete []data_p;
    delete array_p;
    return;
}

inline std::shared_ptr<SHARE_ARRAY> make_shared_array(std::size_t size, const char* file_name, int file_line) {
    auto ret_ptr = std::shared_ptr<SHARE_ARRAY>(new SHARE_ARRAY, delete_array);
    ret_ptr->_data_p = new unsigned char[size + 4];
    
    unsigned char* data_p = ret_ptr->_data_p;
    
    memset(data_p, 0, size);
    data_p[size] = 'a';
    data_p[size+1] = 'b';
    data_p[size+2] = 'c';
    data_p[size+3] = 'd';

    ret_ptr->_data_size = size;
    ret_ptr->_filename = file_name;
    ret_ptr->_fileline = file_line;
    return ret_ptr;
}

}

#endif//MEMEX_H