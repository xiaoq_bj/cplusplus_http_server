#include "thread_pool.h"
#include <iostream>

namespace sw_server {
Thread_Pool::Thread_Pool(int thread_max):_thread_max(thread_max),
    _stop(false)
{
    std::cout << "Thread_Pool thread_max=" << _thread_max << std::endl;
}

Thread_Pool::~Thread_Pool() {

}

void Thread_Pool::on_work() {
    while(!_stop) {
        std::function<void()> thread_func_obj;
        {
            std::unique_lock<std::mutex> lock(_queue_mutex);
            _condition.wait(lock, [=](){
                return _stop || !_task_queue.empty();
            });
            if (_stop && _task_queue.empty()) {
                return;
            }
            thread_func_obj = std::move(_task_queue.front());
            _task_queue.pop();
        }
        
        try {
            thread_func_obj();
        } catch(std::exception e) {
            std::cout << "call function exception: " << e.what() << std::endl;
        }

    }
    return;
}

int Thread_Pool::Start() {
    for (int index = 0; index < _thread_max; index++) {
        _thread_list.emplace_back(std::thread(&Thread_Pool::on_work, this));
    }
    return 0;
}


void Thread_Pool::Stop() {
    if (_stop) {
        return;
    }
    {
        std::unique_lock<std::mutex> lock(_queue_mutex);
        _stop = true;
    }
    
    _condition.notify_all();
    for (std::thread &thread_item : _thread_list) {
        thread_item.join();
    }
    return;
}

}