#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <future>

#define THREAD_MAX_NUM 4

namespace sw_server {
using THREAD_FUNC = std::function<void(void*)>;

class Thread_Pool {
public:
    Thread_Pool(int thread_max);
    ~Thread_Pool();

    int Start();
    void Stop();

    template<class F, class... Args>
    std::future<typename std::result_of<F(Args...)>::type> enqueue(F f, Args... args) {
        using return_type = typename std::result_of<F(Args...)>::type;

        auto task = std::make_shared<std::packaged_task<return_type()> >(std::bind(f, args...));
        std::future<return_type> res = task->get_future();
        {
            std::unique_lock<std::mutex> lock(_queue_mutex);
    
            if (_stop) {
                throw std::runtime_error("enqueue on stopped ThreadPool");
            }
    
            _task_queue.emplace([task](){
                (*task)();
            });
        }
        _condition.notify_one();
        return res;
    }
private:
    void on_work();

private:
    int _thread_max = THREAD_MAX_NUM;
    std::queue< std::function<void()> > _task_queue;
    std::vector< std::thread > _thread_list;
    
    std::mutex _queue_mutex;
    std::condition_variable _condition;
    bool _stop;
};

}
#endif//THREAD_POOL_H