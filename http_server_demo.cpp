#include "logging/logger.h"
#include "net/http_handle.h"
#include "net/tcp_server.h"
#include "pub/memex.h"

const std::string VERSION = "0.1";

static int HTTP_PORT = 7000;

using namespace sw_server;

void http_api_test(http_request& req, std::shared_ptr<http_response> respons_ptr) {
    respons_ptr->writestring("it's ok");
}

void http_api_start(http_request& req, std::shared_ptr<http_response> respons_ptr) {
    //InfoLogf("request: subpath=%s, Content_Length=%d", req._url.c_str(), req._content_length);
    std::string paramStr;
    for (auto item : req._param_map) {
        auto value = item.second;

        for (auto param : value) {
            InfoLogf("param: %s=%s", item.first.c_str(), param.c_str());
            paramStr += " ";
            paramStr += item.first;
            paramStr += "=",
            paramStr += param;
        }
    }
    if (req._content_length > 0) {
        auto body_ptr = MAKE_SHARED_ARRAY(req._content_length+1);
        char* body_p = (char*)(body_ptr->_data_p);

        memcpy(body_p, (char*)req._body_ptr->_data_p, req._content_length);
        body_p[req._content_length] = '\0';

        InfoLogf("request body:%s", body_p);
    }

    std::string ret("it's ok!");

    ret += paramStr;
    ret += "\r\n";

    respons_ptr->writestring(ret);
    return;
}

int main(int argn, char** argv) {
    InitLog("./conf/log.properties");

    InfoLogf("http server version=%s", VERSION.c_str());
    int proc_max=std::thread::hardware_concurrency()*4;
    
    if (proc_max < 4) {
        proc_max = 4;
    }
    std::shared_ptr<tcp_server> server_ptr = std::make_shared<tcp_server>(HTTP_PORT, proc_max);

    http_handle::add_subpath("/api/start", http_api_start);
    http_handle::add_subpath("/api/test", http_api_test);

    server_ptr->run();

    while(true) {
        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
    return 1;
}

